﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace CarDealership.Models
{
    public class ContactForm
    {
        // Your task is to:
        //
        // Make all fields required
        // Ensure the Email field contains an '@' symbol
        // Ensure PhoneNumber is in the format: 1-XXX-XXX-XXXX
        // If the Income is less than 10000 and the PurchaseTimeFrameInMonts is greater than 12,
        // generate a model level area that says 'We don't want your business!'

        [Required(ErrorMessage = "Name is required")]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Purchasing Time Frame")]
        public int PurchaseTimeFrameInMonths { get; set; }

        [Display(Name = "Phone Number")]
        [ProperPhoneNumber(ErrorMessage = "Format = XXX-XXX-XXXX")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Please enter a valid email address")]
        [Display(Name = "Email")]
        [ProperEmail(ErrorMessage = "Please enter a valid email address")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Income")]
        [MinimumIncome(ErrorMessage = "Minimum is $20,000")]
        public decimal? Income { get; set; }

    }

    public class ProperEmail : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return false;
            }
            string email = value.ToString();
            if (email == null)
            {
                return false;
            }
            if (email.Contains('@'))
            {
                return base.IsValid(value);
            }
            return false;
            
        }
    }

    public class MinimumIncome : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return false;
            }
            string income = value.ToString();
            decimal incomeSansCommas = decimal.Parse(income.Replace(",", ""));
            if (incomeSansCommas >= 20000)
            {
                return base.IsValid(value);
            }
            return false;
            
        }
    }

    public class ProperPhoneNumber : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return false;
            }
            string number = value.ToString();
            if (Regex.IsMatch(number, @"\d{ 3}\-\d{ 3}-\d{ 4}"))
            {
                return base.IsValid(value);
            }

            return false;
        }
    }
}