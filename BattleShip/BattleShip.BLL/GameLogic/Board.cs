﻿using System;
using System.Collections.Generic;
using System.Linq;
using BattleShip.BLL.Requests;
using BattleShip.BLL.Responses;
using BattleShip.BLL.Ships;

namespace BattleShip.BLL.GameLogic
{
    public class Board
    {
        private string lineZero;
        private string lineOne;
        private string lineTwo;
        private string lineThree;
        private string lineFour;
        private string lineFive;
        private string lineSix;
        private string lineSeven;
        private string lineEight;
        private string lineNine;
        private string lineTen;
        public Dictionary<Coordinate, ShotHistory> ShotHistory;
        private Ship[] _ships;
        private int _currentShipIndex;

        public string playerName;
        public Board()
        {
            ShotHistory = new Dictionary<Coordinate, ShotHistory>();
            _ships = new Ship[5];
            _currentShipIndex = 0;
        }

        public string[,] boardCoordinates = new string[11, 11] { { " ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }, { "a", " ", " ", " ", " ", " ", " ", " ", " ", " ", "  " }, 
            { "b", " ", " ", " ", " ", " ", " ", " ", " ", " ", "  " }, { "c", " ", " ", " ", " ", " ", " ", " ", " ", " ", "  " }, { "d", " ", " ", " ", " ", " ", " ", " ", " ", " ", "  " }, 
            { "e", " ", " ", " ", " ", " ", " ", " ", " ", " ", "  " }, { "f", " ", " ", " ", " ", " ", " ", " ", " ", " ", "  " }, { "g", " ", " ", " ", " ", " ", " ", " ", " ", " ", "  " }, 
            { "h", " ", " ", " ", " ", " ", " ", " ", " ", " ", "  " }, { "i", " ", " ", " ", " ", " ", " ", " ", " ", " ", "  " }, { "j", " ", " ", " ", " ", " ", " ", " ", " ", " ", "  " } };

        public void DisplayBoard()
        { 
            Console.Clear();
            Console.WriteLine("\n\n\n\t               {0}'s Board               ", playerName);
            Console.WriteLine("\n\t|---------------------------------------------");
            lineZero = $"\t| {boardCoordinates[0, 0]} | {boardCoordinates[0, 1]} | {boardCoordinates[0, 2]} | {boardCoordinates[0, 3]} | {boardCoordinates[0, 4]} | {boardCoordinates[0, 5]} | {boardCoordinates[0, 6]} | {boardCoordinates[0, 7]} | {boardCoordinates[0, 8]} | {boardCoordinates[0, 9]} | {boardCoordinates[0, 10]} |";
            ConsoleWriter(lineZero);
            Console.WriteLine("\n\t|---------------------------------------------");
            lineOne = $"\t| {boardCoordinates[1, 0]} | {boardCoordinates[1, 1]} | {boardCoordinates[1, 2]} | {boardCoordinates[1, 3]} | {boardCoordinates[1, 4]} | {boardCoordinates[1, 5]} | {boardCoordinates[1, 6]} | {boardCoordinates[1, 7]} | {boardCoordinates[1, 8]} | {boardCoordinates[1, 9]} | {boardCoordinates[1, 10]} |";
            ConsoleWriter(lineOne);
            Console.WriteLine("\n\t|---------------------------------------------");
            lineTwo = $"\t| {boardCoordinates[2, 0]} | {boardCoordinates[2, 1]} | {boardCoordinates[2, 2]} | {boardCoordinates[2, 3]} | {boardCoordinates[2, 4]} | {boardCoordinates[2, 5]} | {boardCoordinates[2, 6]} | {boardCoordinates[2, 7]} | {boardCoordinates[2, 8]} | {boardCoordinates[2, 9]} | {boardCoordinates[2, 10]} |";
            ConsoleWriter(lineTwo);
            Console.WriteLine("\n\t|---------------------------------------------");
            lineThree = $"\t| {boardCoordinates[3, 0]} | {boardCoordinates[3, 1]} | {boardCoordinates[3, 2]} | {boardCoordinates[3, 3]} | {boardCoordinates[3, 4]} | {boardCoordinates[3, 5]} | {boardCoordinates[3, 6]} | {boardCoordinates[3, 7]} | {boardCoordinates[3, 8]} | {boardCoordinates[3, 9]} | {boardCoordinates[3, 10]} |";
            ConsoleWriter(lineThree);
            Console.WriteLine("\n\t|---------------------------------------------");
            lineFour = $"\t| {boardCoordinates[4, 0]} | {boardCoordinates[4, 1]} | {boardCoordinates[4, 2]} | {boardCoordinates[4, 3]} | {boardCoordinates[4, 4]} | {boardCoordinates[4, 5]} | {boardCoordinates[4, 6]} | {boardCoordinates[4, 7]} | {boardCoordinates[4, 8]} | {boardCoordinates[4, 9]} | {boardCoordinates[4, 10]} |";
            ConsoleWriter(lineFour);
            Console.WriteLine("\n\t|---------------------------------------------");
            lineFive = $"\t| {boardCoordinates[5, 0]} | {boardCoordinates[5, 1]} | {boardCoordinates[5, 2]} | {boardCoordinates[5, 3]} | {boardCoordinates[5, 4]} | {boardCoordinates[5, 5]} | {boardCoordinates[5, 6]} | {boardCoordinates[5, 7]} | {boardCoordinates[5, 8]} | {boardCoordinates[5, 9]} | {boardCoordinates[5, 10]} |";
            ConsoleWriter(lineFive);
            Console.WriteLine("\n\t|---------------------------------------------");
            lineSix = $"\t| {boardCoordinates[6, 0]} | {boardCoordinates[6, 1]} | {boardCoordinates[6, 2]} | {boardCoordinates[6, 3]} | {boardCoordinates[6, 4]} | {boardCoordinates[6, 5]} | {boardCoordinates[6, 6]} | {boardCoordinates[6, 7]} | {boardCoordinates[6, 8]} | {boardCoordinates[6, 9]} | {boardCoordinates[6, 10]} |";
            ConsoleWriter(lineSix);
            Console.WriteLine("\n\t|---------------------------------------------");
            lineSeven = $"\t| {boardCoordinates[7, 0]} | {boardCoordinates[7, 1]} | {boardCoordinates[7, 2]} | {boardCoordinates[7, 3]} | {boardCoordinates[7, 4]} | {boardCoordinates[7, 5]} | {boardCoordinates[7, 6]} | {boardCoordinates[7, 7]} | {boardCoordinates[7, 8]} | {boardCoordinates[7, 9]} | {boardCoordinates[7, 10]} |";
            ConsoleWriter(lineSeven);
            Console.WriteLine("\n\t|---------------------------------------------");
            lineEight = $"\t| {boardCoordinates[8, 0]} | {boardCoordinates[8, 1]} | {boardCoordinates[8, 2]} | {boardCoordinates[8, 3]} | {boardCoordinates[8, 4]} | {boardCoordinates[8, 5]} | {boardCoordinates[8, 6]} | {boardCoordinates[8, 7]} | {boardCoordinates[8, 8]} | {boardCoordinates[8, 9]} | {boardCoordinates[8, 10]} |";
            ConsoleWriter(lineEight);
            Console.WriteLine("\n\t|---------------------------------------------");
            lineNine = $"\t| {boardCoordinates[9, 0]} | {boardCoordinates[9, 1]} | {boardCoordinates[9, 2]} | {boardCoordinates[9, 3]} | {boardCoordinates[9, 4]} | {boardCoordinates[9, 5]} | {boardCoordinates[9, 6]} | {boardCoordinates[9, 7]} | {boardCoordinates[9, 8]} | {boardCoordinates[9, 9]} | {boardCoordinates[9, 10]} |";
            ConsoleWriter(lineNine);
            Console.WriteLine("\n\t|---------------------------------------------");
            lineTen = $"\t| {boardCoordinates[10, 0]} | {boardCoordinates[10, 1]} | {boardCoordinates[10, 2]} | {boardCoordinates[10, 3]} | {boardCoordinates[10, 4]} | {boardCoordinates[10, 5]} | {boardCoordinates[10, 6]} | {boardCoordinates[10, 7]} | {boardCoordinates[10, 8]} | {boardCoordinates[10, 9]} | {boardCoordinates[10, 10]} |";
            ConsoleWriter(lineTen);
            Console.WriteLine("\n\t|---------------------------------------------\n");
        }

        public void ConsoleWriter(string changeCertainLetters)
        {
            foreach (var c in changeCertainLetters)
            {
                if (c == 'M')
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                }
                if (c == 'H')
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                }
                Console.Write(c);

                Console.ResetColor();
            }
        }

        public FireShotResponse FireShot(Coordinate coordinate)
        {
            var response = new FireShotResponse();

            // is this coordinate on the board?
            if (!IsValidCoordinate(coordinate))
            {
                response.ShotStatus = ShotStatus.Invalid;
                return response;
            }

            // did they already try this position?
            if (ShotHistory.ContainsKey(coordinate))
            {
                response.ShotStatus = ShotStatus.Duplicate;
                return response;
            }

            CheckShipsForHit(coordinate, response);
            CheckForVictory(response);

            return response;            
        }

        private void CheckForVictory(FireShotResponse response)
        {
            if (response.ShotStatus == ShotStatus.HitAndSunk)
            {
                // did they win?
                if (_ships.All(s => s.IsSunk))
                    response.ShotStatus = ShotStatus.Victory;
            }
        }

        private void CheckShipsForHit(Coordinate coordinate, FireShotResponse response)
        {
            response.ShotStatus = ShotStatus.Miss;

            foreach (var ship in _ships)
            {
                // no need to check sunk ships
                if (ship.IsSunk)
                    continue;

                ShotStatus status = ship.FireAtShip(coordinate);

                switch (status)
                {
                    case ShotStatus.HitAndSunk:
                        response.ShotStatus = ShotStatus.HitAndSunk;
                        response.ShipImpacted = ship.ShipName;
                        ShotHistory.Add(coordinate, Responses.ShotHistory.Hit);
                        break;
                    case ShotStatus.Hit:
                        response.ShotStatus = ShotStatus.Hit;
                        response.ShipImpacted = ship.ShipName;
                        ShotHistory.Add(coordinate, Responses.ShotHistory.Hit);
                        break;
                }

                // if they hit something, no need to continue looping
                if (status != ShotStatus.Miss)
                    break;
            }

            if (response.ShotStatus == ShotStatus.Miss)
            {
                ShotHistory.Add(coordinate, Responses.ShotHistory.Miss);
            }
        }

        private bool IsValidCoordinate(Coordinate coordinate)
        {
            return coordinate.XCoordinate >= 1 && coordinate.XCoordinate <= 10 &&
            coordinate.YCoordinate >= 1 && coordinate.YCoordinate <= 10;
        }

        public ShipPlacement PlaceShip(PlaceShipRequest request)
        {
            if (_currentShipIndex > 4)
                throw new Exception("You can not add another ship, 5 is the limit!");

            if (!IsValidCoordinate(request.Coordinate))
                return ShipPlacement.NotEnoughSpace;

            Ship newShip = ShipCreator.CreateShip(request.ShipType);
            switch (request.Direction)
            {
                case ShipDirection.Down:
                    return PlaceShipDown(request.Coordinate, newShip);
                case ShipDirection.Up:
                    return PlaceShipUp(request.Coordinate, newShip);
                case ShipDirection.Left:
                    return PlaceShipLeft(request.Coordinate, newShip);
                default:
                    return PlaceShipRight(request.Coordinate, newShip);
            }
        }

        private ShipPlacement PlaceShipDown(Coordinate coordinate, Ship newShip)
        {
            // x coordinate gets bigger
            int positionIndex = 0;
            int maxX = coordinate.XCoordinate + newShip.BoardPositions.Length;

            for (int i = coordinate.XCoordinate; i < maxX; i++)
            {
                var currentCoordinate = new Coordinate(i, coordinate.YCoordinate);

                if (!IsValidCoordinate(currentCoordinate))
                    return ShipPlacement.NotEnoughSpace;

                if (OverlapsAnotherShip(currentCoordinate))
                    return ShipPlacement.Overlap;

                newShip.BoardPositions[positionIndex] = currentCoordinate;
                positionIndex++;
            }

            AddShipToBoard(newShip);
            return ShipPlacement.Ok;
        }

        private ShipPlacement PlaceShipUp(Coordinate coordinate, Ship newShip)
        {
            // x coordinate gets smaller
            int positionIndex = 0;
            int minX = coordinate.XCoordinate - newShip.BoardPositions.Length;

            for (int i = coordinate.XCoordinate; i > minX; i--)
            {
                var currentCoordinate = new Coordinate(i, coordinate.YCoordinate);

                if (!IsValidCoordinate(currentCoordinate))
                    return ShipPlacement.NotEnoughSpace;

                if (OverlapsAnotherShip(currentCoordinate))
                    return ShipPlacement.Overlap;

                newShip.BoardPositions[positionIndex] = currentCoordinate;
                positionIndex++;
            }

            AddShipToBoard(newShip);
            return ShipPlacement.Ok;
        }

        private ShipPlacement PlaceShipLeft(Coordinate coordinate, Ship newShip)
        {
            // y coordinate gets smaller
            int positionIndex = 0;
            int minY = coordinate.YCoordinate - newShip.BoardPositions.Length;

            for (int i = coordinate.YCoordinate; i > minY; i--)
            {
                var currentCoordinate = new Coordinate(coordinate.XCoordinate, i);

                if (!IsValidCoordinate(currentCoordinate))
                    return ShipPlacement.NotEnoughSpace;

                if (OverlapsAnotherShip(currentCoordinate))
                    return ShipPlacement.Overlap;

                newShip.BoardPositions[positionIndex] = currentCoordinate; 
                positionIndex++;
            }

            AddShipToBoard(newShip);
            return ShipPlacement.Ok;
        }

        private ShipPlacement PlaceShipRight(Coordinate coordinate, Ship newShip)
        {
            // y coordinate gets bigger
            int positionIndex = 0;
            int maxY = coordinate.YCoordinate + newShip.BoardPositions.Length;
            
            for (int i = coordinate.YCoordinate; i < maxY; i++)
            {
                var currentCoordinate = new Coordinate(coordinate.XCoordinate, i);
                if (!IsValidCoordinate(currentCoordinate))
                    return ShipPlacement.NotEnoughSpace;

                if (OverlapsAnotherShip(currentCoordinate))
                    return ShipPlacement.Overlap;

                newShip.BoardPositions[positionIndex] = currentCoordinate;
                positionIndex++;
            }

            AddShipToBoard(newShip);
            return ShipPlacement.Ok;
        }

        private void AddShipToBoard(Ship newShip)

        {
            _ships[_currentShipIndex] = newShip;
            _currentShipIndex++;
        }

        private bool OverlapsAnotherShip(Coordinate coordinate)
        {
            foreach (var ship in _ships)
            {
                if (ship != null)
                {
                    if (ship.BoardPositions.Contains(coordinate))
                        return true;
                }
            }

            return false;
        }
    }
}

