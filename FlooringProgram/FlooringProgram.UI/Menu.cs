﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.SqlServer.Server;
using System.Configuration;
using System.Linq.Expressions;

namespace FlooringProgram.UI
{
    internal class Menu
    {
        public static string input;
        private static string currentMode;
        private static string currentFormat;

        private static string DisplayMode()
        {
            if (ConfigurationManager.AppSettings["mode"] == "Prod")
            {
                return "production";
            }

            return "test";
        }

        private static string DisplayFormat()
        {
            if (ConfigurationManager.AppSettings["FileType"] == "csv")
            {
                return "CSV";
            }
            return "JSON";
        }

        public static string MenuText =
            @"
   *******************************************************************************
   *    /$$$$$$$$/$$                           /$$                               *
   *   | $$_____| $$                          |__/                               *
   *   | $$     | $$ /$$$$$$  /$$$$$$  /$$$$$$ /$$/$$$$$$$  /$$$$$$              *
   *   | $$$$$  | $$/$$__  $$/$$__  $$/$$__  $| $| $$__  $$/$$__  $$             *
   *   | $$__/  | $| $$  \ $| $$  \ $| $$  \__| $| $$  \ $| $$  \ $$             *
   *   | $$     | $| $$  | $| $$  | $| $$     | $| $$  | $| $$  | $$             *
   *   | $$     | $|  $$$$$$|  $$$$$$| $$     | $| $$  | $|  $$$$$$$             *
   *   |__/     |__/\______/ \______/|__/     |__|__/  |__/\____  $$             *
   *                                                       /$$  \ $$             *
   *               PROGRAM                                 |  $$$$$$/            *
   *                                                        \______/             *
   *                                                                             *
   *   1. Display Orders                                                         *
   *   2. Add an Order                                                           *
   *   3. Edit an Order                                                          *
   *   4. Remove an Order                                                        *
   *   5. Change Program Mode                                                    *
   *   6. Change File Mode                                                       *
   *   7. Quit                                                                   *
   *                                                                             *
   *******************************************************************************";

        public static void DisplayMenu()
        {
            currentMode = DisplayMode();
            currentFormat = DisplayFormat();
            Console.WriteLine(MenuText);
            Console.WriteLine(
                $"    **You are in {currentMode} mode and your orders are saving in the {currentFormat} format.** \n");
        }




        // prompt user to choose menu option and call appropriate function
        public static void MenuPrompt()
        {


            while (true)
            {
                Console.Clear();
                DisplayMenu();

                Console.Write("\n\n\t\tEnter an option: ");

                input = Console.ReadLine();
                input = input.Replace(" ", "");

                try
                {

                    switch (input) // Ensure that input is 1-5 and call appropriate function
                    {
                        case "1":
                            Program.DisplayOrder();
                            break;
                        case "2":
                            Program.AddOrder();
                            break;
                        case "3":
                            Program.EditOrder();
                            break;
                        case "4":
                            Program.RemoveOrder();
                            break;
                        case "5":
                            Program.ModePrompt();
                            break;
                        case "6":
                            Program.FileFormatPrompt();
                            break;
                        case "7":
                            return;
                        default:
                            Console.SetCursorPosition(0, Console.CursorTop - 3);
                            Prompt.ErrorWriter("\t\tInvalid entry. Press Any key to Try again.");
                            Console.ReadKey();
                            break;
                    }
                }
                catch
                {
                    Prompt.ErrorWriter(
                        "\n\t\tAn error occurred and has been logged.\n\t\tPress any key to return to menu.");
                    Console.ReadKey();
                }
            }
        }
    }
}

