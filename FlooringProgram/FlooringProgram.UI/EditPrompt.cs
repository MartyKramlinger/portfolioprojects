﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FlooringProgram.Models;
using FlooringProgram.Operations;

namespace FlooringProgram.UI
{
    // contains order prompts from Prompt class, but calls sendkeys on the original property data (enabling user editing)
    public class EditPrompt
    {
        public static TaxRate StateTaxPrompt(Order order)
        {

            Console.WriteLine("\n\t\t\tEnter Order Details");
            Console.Write($"\n\n\n\t\tState: ");
            SendKeys.SendWait(order.StateTaxRate.State);

            while (true)
            {

                string state = Console.ReadLine().Replace(" ", "");

                TaxRateOperations taxOps = new TaxRateOperations();

                if (taxOps.IsAllowedState(state))
                {
                    Console.Clear();
                    return taxOps.GetTaxRateFor(state);

                }


                Console.Clear();
                Console.WriteLine("\n\t\t\tEnter Order Details");
                Prompt.ErrorWriter("\n\t\tInvalid state. Please try again.");
                Console.Write($"\n\n\t\tState: ");
                SendKeys.SendWait(order.StateTaxRate.State);

            }

        }

        public static double AreaPrompt(Order order)
        {

            Console.WriteLine("\n\t\t\tEnter Order Details");
            Console.Write($"\n\n\n\t\tArea: ");
            SendKeys.SendWait(Convert.ToString(order.Area));

            while (true)
            {

                string areaStr = Console.ReadLine();

                double area;



                if (!string.IsNullOrEmpty(areaStr) && double.TryParse(areaStr, out area))
                {
                    Console.Clear();
                    return area;
                }


                Console.Clear();
                Console.WriteLine("\n\t\t\tEnter Order Details");
                Prompt.ErrorWriter("\n\t\tInvalid entry. Please try again.");
                Console.Write($"\n\n\t\tArea: ");
                SendKeys.SendWait(Convert.ToString(order.Area));
            }

        }

        public static string CustomerPrompt(Order order)
        {

            Console.WriteLine("\n\t\t\tEnter Order Details");
            Console.Write($"\n\n\n\t\tCustomer Name: ");
            SendKeys.SendWait(order.CustomerName);

            while (true)
            {

                string name = Console.ReadLine();

                if (!(name.Replace(" ", "") == ""))
                {
                    Console.Clear();
                    return name;
                }
                Console.Clear();
                Console.WriteLine("\n\t\t\tEnter Order Details");
                Prompt.ErrorWriter("\n\t\tInvalid entry. Please try again.");
                Console.Write($"\n\n\t\tCustomer Name: ");
                SendKeys.SendWait(order.CustomerName);
            }

        }

        public static Product ProductPrompt(Order order)
        {
            Product customerProduct = new Product();
            Console.WriteLine("\n\t\t\tEnter Order Details");
            Console.Write($"\n\n\n\t\tProduct Name: ");
            SendKeys.SendWait(order.ProductInfo.ProductType);

            while (true)
            {

                string productName = Console.ReadLine();

                if (!string.IsNullOrEmpty(productName))
                {
                    if (ProductOperations.GetProduct(productName, ref customerProduct))
                    {
                        Console.Clear();
                        return customerProduct;
                    }

                }
                Console.Clear();
                Console.WriteLine("\n\t\t\tEnter Order Details");
                Prompt.ErrorWriter("\n\t\tInvalid entry. Please try again.");
                Console.Write($"\n\n\t\tProduct Name: ");
                SendKeys.SendWait(order.ProductInfo.ProductType);

            }
        }
    }
}
