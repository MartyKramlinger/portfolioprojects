﻿using System;
using System.Threading;
using FlooringProgram.Models;
using FlooringProgram.Operations;

namespace FlooringProgram.UI
{
    class Prompt
    {

        static bool isValidDate;
        static bool isValidOrderNumber;

        //makes user say whic of the allows states they live in
        public static TaxRate StateTaxPrompt()
        {

            Console.WriteLine("\n\t\t\tEnter Order Details");
            Console.Write($"\n\n\n\t\tState: ");

            while (true)
            {

                string state = Console.ReadLine().Replace(" ", "").ToUpper();

                TaxRateOperations taxOps = new TaxRateOperations();

                if (taxOps.IsAllowedState(state))
                {
                    Console.Clear();
                    return taxOps.GetTaxRateFor(state);

                }
                
                Console.Clear();
                Console.WriteLine("\n\t\t\tEnter Order Details");
                ErrorWriter("\n\t\tInvalid state. Please try again.");
                Console.Write($"\n\n\t\tState: ");

            }

        }

        //gets area from the user
        public static double AreaPrompt()
        {
            Console.WriteLine("\n\t\t\tEnter Order Details");
            Console.Write($"\n\n\n\t\tArea: ");

            while (true)
            {
                string areaStr = Console.ReadLine();
                double area;
                
                if (!string.IsNullOrEmpty(areaStr) && double.TryParse(areaStr, out area))
                {
                    Console.Clear();
                    return area;
                }
                
                Console.Clear();
                Console.WriteLine("\n\t\t\tEnter Order Details");
                ErrorWriter("\n\t\tInvalid entry. Please try again.");
                Console.Write($"\n\n\t\tArea: ");

            }
        }

        //asks the user for their name
        public static string CustomerPrompt()
        {
            Console.WriteLine("\n\t\t\tEnter Order Details");
            Console.Write($"\n\n\n\t\tCustomer Name: ");

            while (true)
            {
                string name = Console.ReadLine();

                if (!(name.Replace(" ", "") == ""))
                {
                    Console.Clear();
                    return name;
                }
                Console.Clear();
                Console.WriteLine("\n\t\t\tEnter Order Details");
                ErrorWriter("\n\t\tInvalid entry. Please try again.");
                Console.Write($"\n\n\t\tCustomer Name: ");

            }
        }

        //makes user choose between the four products and returns that product
        public static Product ProductPrompt()
        {
            Product customerProduct = new Product();
            Console.WriteLine("\n\t\t\tEnter Order Details");
            Console.Write($"\n\n\n\t\tProduct Name: ");

            while (true)
            {
                string productName = Console.ReadLine();

                if (!string.IsNullOrEmpty(productName))
                {
                    if (ProductOperations.GetProduct(productName, ref customerProduct))
                    {
                        Console.Clear();
                        return customerProduct;
                    }

                }
                Console.Clear();
                Console.WriteLine("\n\t\t\tEnter Order Details");
                ErrorWriter("\n\t\tInvalid entry. Please try again.");
                Console.Write($"\n\n\t\tProduct Name: ");

            }

        }

        //takes user input and returns true if the user wants to save their order, and false if not
        public static bool SaveFilePrompt()
        {
            Console.Write("\n\n\n\t\tWould You like to save your order? (Y/N): ");
            
            while (true)
            {
                string input = Console.ReadLine().Replace(" ", "");

                if (input.ToUpper() == "Y")
                {
                    return true;
                }
                if (input.ToUpper() == "N")
                {

                    return false;
                }
                

                DeleteLine(3);
                Console.SetCursorPosition(0, Console.CursorTop - 4);
                ErrorWriter("\n\t\tInvalid entry. Please try again.\n\n");

                DeleteLine(0);

                Console.Write("\t\tWould You like to save your order? (Y/N): ");
                
            }

        }

        //gets and validates the date and order number from the user. parameters must be declared before
        //before the method is called, since the method also alters those values to proper formating
        public static void PromptOrderInfo(out string orderDate, out int orderNumber)
        {
            do
            {
                Console.Write("\n\t\tEnter a date in the format of (ddmmyyy): ");
                orderDate = Console.ReadLine();

                isValidDate = Validation.ValidateDate(ref orderDate);
                if (isValidDate)
                {
                    continue;
                }
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("\tInvalid Date. Please try again.\n");
                Console.ResetColor();

            } while (!isValidDate);

            do
            {
                Console.Write("\n\t\tEnter an order number: ");
                string orderNumberStr = Console.ReadLine();
                orderNumberStr = orderNumberStr.Replace(" ", "");

                isValidOrderNumber = int.TryParse(orderNumberStr, out orderNumber);
                if (isValidOrderNumber)
                {
                    continue;
                }
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("\tInvalid order number. Please try again.");
                Console.ResetColor();

            } while (!isValidOrderNumber);
        }

        //takes in a string and displays it in red
        public static void ErrorWriter(string str)
        {
            foreach (var c in str)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write(c);
                Thread.Sleep(10);
                Console.ResetColor();
            }
        }

        //deletes a certain amount of lines so the input area can be 
        //cleared without clearing the rest of the screen
        public static void DeleteLine(params int[] linesAbove)
        {
            foreach (int n in linesAbove)
            {
                int returnLineCursor = Console.CursorTop;

                Console.SetCursorPosition(0, Console.CursorTop - n);
                Console.WriteLine(new string(' ', Console.WindowWidth));

                Console.SetCursorPosition(0, returnLineCursor);
            }

        }

        //the prompt when the user wants to delete an order
        public static bool DeleteOrderPrompt()
        {
            Console.Write("\n\n\n\t\tThis is the order.  Are you sure you want to delete it? (Y/N): ");

            while (true)
            {
                string input = Console.ReadLine().Replace(" ", "");

                if (input.ToUpper() == "Y")
                {
                    return true;
                }
                if (input.ToUpper() == "N")
                {

                    return false;
                }
                
                DeleteLine(3);
                Console.SetCursorPosition(0, Console.CursorTop - 4);
                ErrorWriter("\n\t\tInvalid entry. Please try again.\n\n");
                DeleteLine(0);

                Console.Write("\t\tWould You like to delete your order? (Y/N): ");

            }

        }

    }
}
