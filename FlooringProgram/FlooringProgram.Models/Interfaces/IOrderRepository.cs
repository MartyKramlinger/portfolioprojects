﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlooringProgram.Models.Interfaces
{
    public interface IOrderRepository
    {
        List<Order> GetOrders(string date);

        bool Save(List<Order> orderList, string date);

        bool CheckForFile(string date);

        Order OrderReader(string date, int orderNumber);

        int LineCounter(string date);

        bool DeleteFile(string date);
    }
}
