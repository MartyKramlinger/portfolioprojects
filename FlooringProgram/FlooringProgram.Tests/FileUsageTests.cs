﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Models;
using FlooringProgram.Operations;
using NUnit.Framework;

namespace FlooringProgram.Tests
{
    class FileUsageTests
    {

        [TestCase("Carpet", true)]
        [TestCase("Wood", true)]
        [TestCase("Human tears", false)]
        public static void GetProductTest(string productStr, bool expected)
        {
            Product product = new Product();

            bool actual = ProductOperations.GetProduct(productStr, ref product);
            
            Assert.AreEqual(expected, actual);
        }


        [Test]
        public static void GetOrderNumber()
        {
            //ConfigurationManager.AppSettings["mode"] = "Prod";

            int orderNumber = OrderOperations.GetOrderNumber("09222015");
            //at the moment the method is equal to zero 
            //since there are no orders in the 09222015 file
            Assert.AreEqual(orderNumber, 0);
        }

    }
}
