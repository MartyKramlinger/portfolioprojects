﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Models;
using FlooringProgram.Models.Interfaces;

namespace FlooringProgram.Data.Products
{

    public class ProductTestRepository : IProductRepository
    {

        public static List<Product> TestProducts = new List<Product>()
        {
            new Product()
            {
                ProductType = "Carpet",
                MaterialCostPerSquareFoot = 2.25M,
                LaborCostPerSquareFoot = 2.10M,
            },
            new Product()
            {
                ProductType = "Laminate",
                MaterialCostPerSquareFoot = 1.75M,
                LaborCostPerSquareFoot = 2.10M,
            },
            new Product()
            {
                ProductType = "Tile",
                MaterialCostPerSquareFoot = 3.50M,
                LaborCostPerSquareFoot = 4.15M,
            },
            new Product()
            {
                ProductType = "Wood",
                MaterialCostPerSquareFoot = 5.15M,
                LaborCostPerSquareFoot = 4.75M,
            }
        };

        public bool ProductReader(string productType, ref Product product)
        {
            var products = TestProducts.Where(p => p.ProductType.ToLower() == productType.ToLower());

            if (products.Any())
            {
                product = products.First();
                return true;
            }
            return false;
            
        }
    }
}
