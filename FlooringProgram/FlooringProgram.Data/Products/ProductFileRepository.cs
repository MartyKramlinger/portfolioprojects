﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Models;
using FlooringProgram.Models.Interfaces;

namespace FlooringProgram.Data.Products
{
    public class ProductFileRepository : IProductRepository
    {
        // return product with designated product type from products.txt
        public bool ProductReader(string productType, ref Product product)
        {
            try
            {
                string[] data = File.ReadAllLines(@"Data\Products.txt");

                for (int i = 1; i < data.Length; i++)
                {
                    string[] row = data[i].Split(',');
                    if (row[0].ToLower() == productType.ToLower())
                    {
                        product.ProductType = row[0];
                        product.MaterialCostPerSquareFoot = Convert.ToDecimal(row[1]);
                        product.LaborCostPerSquareFoot = Convert.ToDecimal(row[2]);
                        return true;
                    }
                }

                return false;
            }
            catch
            {
                return false; //TODO ? - save log file
            }

        }
    }
}
