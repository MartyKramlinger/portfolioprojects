﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FlooringProgram.Data
{
    public class ErrorLogger
    {
        public static void LogException(Exception ex)
        {
            string filePath = @"Data\ErrorLog.txt";
            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                writer.WriteLine("Message: " + ex.Message + Environment.NewLine + 
                                 "Date: " + DateTime.Now + Environment.NewLine + 
                                 "StackTrace: " + ex.StackTrace);

                writer.WriteLine(Environment.NewLine + new string ('-', 80)  + Environment.NewLine);

            }

        }

    }
}
