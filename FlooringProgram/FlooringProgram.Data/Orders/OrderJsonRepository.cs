﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Models;
using FlooringProgram.Models.Interfaces;
using System.IO;
using Newtonsoft.Json;
using System.Configuration;

namespace FlooringProgram.Data.Orders
{
    public class OrderJsonRepository : IOrderRepository
    {

        private static string FilePath(string date)
        {
            return $@"Data\Orders_{date}.json";
        }

        // Read file at designated filePath, set order object corresponding to order number, 
        // and return bool indicating whether order exists
        public Order OrderReader(string date, int orderNumber)
        {
            try
            {
                
                Order order = new Order(); 

                string jsonFile = File.ReadAllText(FilePath(date));
                List<Order> json = JsonConvert.DeserializeObject<List<Order>>(jsonFile);

                order = json.First(o => o.OrderNumber == orderNumber);
                
                return order;
                
            }
            catch (Exception ex)
            {
                ErrorLogger.LogException(ex);
                throw ex;

            }

        }

        public int LineCounter(string date)
        {
            try
            {
                    string jsonFile = File.ReadAllText(FilePath(date));
                    List<Order> json = JsonConvert.DeserializeObject<List<Order>>(jsonFile);
                    return json.Count() + 1;
            }
            catch (Exception ex)
            {

                ErrorLogger.LogException(ex);
                throw ex;
            }

        }

        public List<Order> GetOrders(string date)
        {
            List<Order> orderList = new List<Order>();

            try
            {
                string jsonFile = File.ReadAllText(FilePath(date));
                List<Order> json = JsonConvert.DeserializeObject<List<Order>>(jsonFile);

                return json;
                
            }
            catch (Exception ex)
            {
                ErrorLogger.LogException(ex);
                throw ex;
            }
        }

        public bool Save(List<Order> orderList, string date)
        {
         
            try
            {
                string json = JsonConvert.SerializeObject(orderList.ToArray(), Formatting.Indented);
                File.WriteAllText(FilePath(date), json);
                
                return true;
            }
            catch (Exception ex)
            {
                ErrorLogger.LogException(ex);
                throw ex;
            }
        }


        public bool CheckForFile(string date)
        {
            
            return File.Exists(FilePath(date));

        }

        public bool DeleteFile(string date)
        {
      
            try
            {
                File.Delete(FilePath(date));
                return true;
            }
            catch (Exception ex)
            {
                ErrorLogger.LogException(ex);
                throw ex;
            }

        }
    }
}
