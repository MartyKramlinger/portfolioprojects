﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Models;
using FlooringProgram.Models.Interfaces;
using Newtonsoft.Json;

namespace FlooringProgram.Data.Orders
{
    public class OrderCsvRepository : IOrderRepository
    {
        private static string FilePath(string date)
        {
            return $@"Data\Orders_{date}.txt";
        }

        // Read file at designated filePath, set order object corresponding to order number, 
        // and return bool indicating whether order exists
        public Order OrderReader(string date, int orderNumber)
        {
            try
            {
                Order order = new Order();

                string[] data = File.ReadAllLines(FilePath(date));

                if (data.Length <= orderNumber)
                {
                    return null;
                }

             
                // If order exists set values to order fields 
                string[] row = data[orderNumber].Split(',');

                order.OrderNumber = Convert.ToInt16(row[0]);
                order.CustomerName = row[1];
                order.StateTaxRate = new TaxRate()
                {
                    State = row[2],
                    TaxPercent = Convert.ToDecimal(row[3])
                };
                order.Area = Convert.ToDouble(row[5]);
                order.ProductInfo = new Product()
                {
                    ProductType = row[4],
                    LaborCostPerSquareFoot = Convert.ToDecimal(row[6]),
                    MaterialCostPerSquareFoot = Convert.ToDecimal(row[7])
                };
                order.MaterialCost = Convert.ToDecimal(row[8]);
                order.LaborCost = Convert.ToDecimal(row[9]);
                order.Tax = Convert.ToDecimal(row[10]);
                order.Total = Convert.ToDecimal(row[11]);
                
                return order;

            }
            catch (Exception ex)
            {
                ErrorLogger.LogException(ex);

                throw ex;
                //return null; //TODO ? - save log file

            }

        }

        public int LineCounter(string date)
        {
            try
            {
                string[] data = File.ReadAllLines(FilePath(date));

                return data.Length;
                
            }
            catch
            {
                
                return 0; //TODO ? - save log file & return error data 
            }

        }

        public List<Order> GetOrders(string date)
        {
            List<Order> orderList = new List<Order>();

            try
            {
                string[] data = File.ReadAllLines(FilePath(date));

                for (int i = 1; i < data.Length; i++)
                {
                    string[] row = data[i].Split(',');
                    Order order = new Order
                    {
                        OrderNumber = Convert.ToInt16(row[0]),
                        CustomerName = row[1],
                        StateTaxRate = new TaxRate()
                        {
                            State = row[2],
                            TaxPercent = Convert.ToDecimal(row[3])
                        },
                        Area = Convert.ToDouble(row[5]),
                        ProductInfo = new Product()
                        {
                            ProductType = row[4],
                            LaborCostPerSquareFoot = Convert.ToDecimal(row[6]),
                            MaterialCostPerSquareFoot = Convert.ToDecimal(row[7])
                        },
                        MaterialCost = Convert.ToDecimal(row[8]),
                        LaborCost = Convert.ToDecimal(row[9]),
                        Tax = Convert.ToDecimal(row[10]),
                        Total = Convert.ToDecimal(row[11])
                    };

                    orderList.Add(order);

                }

                return orderList;
                
            }
            catch(Exception ex)
            {
                ErrorLogger.LogException(ex);
                throw ex; 

            }
        }

        public bool Save(List<Order> orderList, string date)
        {
            try
            {
                using (File.Create(FilePath(date)))
                {
                }
                using (StreamWriter sw = new StreamWriter(FilePath(date)))
                {
                    sw.WriteLine("OrderNumber,CustomerName,State,TaxRate,ProductType,Area,CostPerSquareFoot," +
                                 "LaborCostPerSquareFoot,MaterialCost,LaborCost,Tax,Total");
                    foreach (Order odr in orderList)
                    {
                        sw.Write(odr.OrderNumber + ",");
                        sw.Write(odr.CustomerName + ",");
                        sw.Write(odr.StateTaxRate.State + ",");
                        sw.Write(odr.StateTaxRate.TaxPercent + ",");
                        sw.Write(odr.ProductInfo.ProductType + ",");
                        sw.Write(odr.Area + ",");
                        sw.Write(odr.ProductInfo.MaterialCostPerSquareFoot + ",");
                        sw.Write(odr.ProductInfo.LaborCostPerSquareFoot + ",");
                        sw.Write(odr.MaterialCost + ",");
                        sw.Write(odr.LaborCost + ",");
                        sw.Write(odr.Tax + ",");
                        sw.Write(odr.Total + Environment.NewLine);
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                ErrorLogger.LogException(ex);
                throw ex;

            }
        }

        public bool CheckForFile(string date)
        {
            return File.Exists(FilePath(date));
        }

        public bool DeleteFile(string date)
        {
            try 
            {
                File.Delete(FilePath(date));
                return true;
            }
            catch(Exception ex)
            {
 
                ErrorLogger.LogException(ex);
                throw ex;
            }
            
        }

    }
}
