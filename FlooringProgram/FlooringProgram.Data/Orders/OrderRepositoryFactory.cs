﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Models.Interfaces;
using System.Configuration;

namespace FlooringProgram.Data.Orders
{

    public class OrderRepositoryFactory
    {
        public static IOrderRepository GetOrderRepository()
        {
            if (ConfigurationSettings.GetMode() == "Prod")
            {
                if (ConfigurationManager.AppSettings["FileType"] == "csv")
                {
                    return new OrderCsvRepository();
                }
                else
                {
                    return new OrderJsonRepository();
                }     
            }

            if (ConfigurationSettings.GetMode() == "Test")
            {
                return new OrderTestRepository();
            }
            
            return null;
        }
    }

}
