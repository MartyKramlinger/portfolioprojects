﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Data;
using FlooringProgram.Data.Products;
using FlooringProgram.Models;
using FlooringProgram.Models.Interfaces;

namespace FlooringProgram.Operations
{
    public class ProductOperations
    {

        // returns Product with specified type
        public static bool GetProduct(string productType, ref Product productOutput)
        {
            IProductRepository productRepo = ProductRepositoryFactory.GetProductRepository();

            productType = productType.Replace(" ", "");
            return productRepo.ProductReader(productType, ref productOutput);
        }

 
    }
}
