﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Models;

namespace FlooringProgram.Operations
{
    public class TotalsCalc
    {

        //orderToAdd.Tax = TotalsCalc.TaxCalculator(orderToAdd);                Marty
        //orderToAdd.LaborCost = TotalsCalc.LaborCostCalculator(orderToAdd);    Marty

        public static decimal TaxCalculator(Order order)
        {
            return order.StateTaxRate.TaxPercent * (order.LaborCost + order.MaterialCost);
        }
        //orderToAdd.LaborCost = TotalsCalc.LaborCostCalculator(orderToAdd);    Marty
        public static decimal LaborCostCalculator(Order order)
        {
            return (decimal)order.Area * order.ProductInfo.LaborCostPerSquareFoot;
        }


        public static decimal MaterialCostCalculator(Order order)
        {
            return order.ProductInfo.MaterialCostPerSquareFoot * (decimal)order.Area; 
        }

        public static decimal TotalCalculatorCalculator(Order order)
        {
            return order.MaterialCost + order.LaborCost + order.Tax;
        }
    }

   

}
