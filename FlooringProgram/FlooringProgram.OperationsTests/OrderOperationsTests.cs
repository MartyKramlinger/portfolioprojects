﻿using FlooringProgram.Operations;
using FlooringProgram.Data.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Threading.Tasks;
using NUnit.Framework;
using System.IO;

namespace FlooringProgram.Operations.Tests
{
    [TestFixture]
    public class OrderOperationsTests
    {
        bool functionsProperly;

        //
        [Test]
        public void RemoveOrderTest()
        {
            ConfigurationManager.AppSettings["mode"] = "Test";

            OrderCsvRepository newRepository = new OrderCsvRepository();

            int fileLengthBefore = newRepository.LineCounter("09232015");

            functionsProperly = OrderOperations.RemoveOrder("09232015", 3);

            int fileLengthAfter = newRepository.LineCounter("09232015");

            Assert.AreNotEqual(fileLengthBefore, fileLengthAfter);

        }
    }
}