﻿
using FlooringProgram.Data.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using FlooringProgram.Models;

namespace FlooringProgram.Data.Orders.Tests
{

    [TestFixture]
    public class OrderFileRepositoryTests
    {
        OrderCsvRepository TestFileRepository = new OrderCsvRepository();
        //[Test]
        //public void SaveTest()
        //{
        //    bool itWorked;
        //    int afterSave;
        //    beforeSave = FileTracker.LineCounter($@"Data\Orders_09242015.txt");
        //    OrderFileRepository TestFileRepository = new OrderFileRepository();
        //    itWorked = TestFileRepository.Save(OrderTestRepository.TestOrders, $@"Data\Orders_09242015.txt");

        //    afterSave = FileTracker.LineCounter($@"Data\Orders_09242015.txt");

        //    Assert.IsTrue(itWorked);
        //    Assert.AreNotEqual(2, afterSave);

        //}

        [Test]
        public void GetOrders()
        {
            int numOrders;
            List<Order> sampleOrders = TestFileRepository.GetOrders($@"Data\Orders_09242015.txt");
            numOrders = sampleOrders.Count();
            Assert.NotNull(sampleOrders);
            Assert.AreNotEqual(0, numOrders);
        }
    }
}